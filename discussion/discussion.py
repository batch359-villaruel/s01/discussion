# Comments
# Comments in python are done using the "#" symbol
# ctrl + / -> one-line comment

"""
#although we have no keybind for multi-line comment, it is still possible through the use of 2 sets of double quotation marks
"""
# Python Syntax
# Hello world in Python
print("Hello world!")

# Indentation
# wherein other programming language
# In Python, indentation is used to indicate a block of code.